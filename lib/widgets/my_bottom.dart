import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyBottom extends StatelessWidget {
  final void Function() function;
  final String title;

  const MyBottom({Key? key, required this.function, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: function,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(top: 10),
        decoration: const BoxDecoration(
          color: Colors.black38,
          borderRadius: BorderRadius.all(Radius.circular(20))
        ),
        child: Text(title),
      ),
    );
  }
}
