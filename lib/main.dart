import 'package:flutter/material.dart';

import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive/hive.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:hive_tut/models/timedetail_model.dart';
import 'package:hive_tut/pages/home_screen.dart';
import 'package:hive_tut/widgets/my_bottom.dart';
import 'package:path_provider/path_provider.dart';

import 'models/note_model.dart';


const String noteBoxName = 'noteBoxName';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (kIsWeb) {
    Hive.initFlutter();
  } else {
    final document = await getApplicationDocumentsDirectory();
    Hive.init(document.path);
  }

  Hive.registerAdapter(NoteAdapter());
  Hive.registerAdapter(TimeDetailAdapter());

  await Hive.openBox<Note>(noteBoxName);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home:  HomeScreen(),
    );
  }
}

