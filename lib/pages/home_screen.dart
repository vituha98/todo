import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive_tut/models/note_model.dart';

import '../main.dart';
import 'note_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Box<Note> noteBox = Hive.box<Note>(noteBoxName);
    final width = MediaQuery.of(context).size.width;
    final heigh = MediaQuery.of(context).size.height;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: ((context) => NoteScreen())));
        },
        child: Icon(Icons.add),
      ),
      body: Container(
        height: heigh,
        width: width,
        child: SafeArea(
          child: Column(
            children: [
              const Center(
                child: Text('Your Notes!', style: TextStyle(fontSize: 25)),
              ),
              ValueListenableBuilder(
                  valueListenable: noteBox.listenable(),
                  builder: (context, Box<Note> noteBox, _) {
                    return noteBox.isEmpty
                        ? Container()
                        : ListView.builder(
                            shrinkWrap: true,
                            itemCount: noteBox.length,
                            itemBuilder: (context, index) {
                              var noteItem = noteBox.getAt(index);
                              return Container(
                                margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
                                child: ListTile(
                                  tileColor: Colors.amber,
                                  title: Text(noteItem!.title),
                                  subtitle: Text(
                                    noteItem.desc,
                                    softWrap: false,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  trailing: Column(
                                    children: [
                                      Text(noteItem.tags
                                          .toString()
                                          .replaceAll(']', '')
                                          .replaceAll('[', '')),
                                      Spacer(),
                                      Text(
                                        'Created : ${noteItem.timeDetail.createdAt}',
                                        softWrap: false,
                                        overflow: TextOverflow.ellipsis,
                                        style: const TextStyle(fontSize: 10),
                                      ),
                                      Text(
                                          'Updated : ${noteItem.timeDetail.updatedAt}',
                                          softWrap: false,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(fontSize: 10))
                                    ],
                                  ),
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return NoteScreen(
                                        noteItem: noteItem,
                                        noteIndex: index,
                                      );
                                    }));
                                  },
                                ),
                              );
                            });
                  })
            ],
          ),
        ),
      ),
    );
  }
}
