import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';
import 'package:hive_tut/main.dart';
import 'package:hive_tut/models/note_model.dart';
import 'package:hive_tut/models/timedetail_model.dart';
import 'package:hive_tut/widgets/my_bottom.dart';


class NoteScreen extends StatefulWidget {
  final int? noteIndex;
  final Note? noteItem;

  const NoteScreen({Key? key, this.noteIndex, this.noteItem})
      : super(key: key);

  @override
  State<NoteScreen> createState() => _NoteScreenState();
}

class _NoteScreenState extends State<NoteScreen> {
  final _titleContorller = TextEditingController(text: '');
  final _descContorller = TextEditingController(text: '');
  final _tagContorller = TextEditingController(text: '');
  String? timeDetail;
  Note? noteItem;

  @override
  void initState() {
    noteItem = widget.noteItem;
    if (noteItem != null) {
      _titleContorller.text = noteItem!.title;
      _descContorller.text = noteItem!.desc;
      _tagContorller.text =
          noteItem!.tags.toString().replaceAll("]", "").replaceAll('[', '');
      timeDetail =
          'Created : ${noteItem!.timeDetail.createdAt} Updated : ${noteItem!.timeDetail.updatedAt}';
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Box<Note> noteBox = Hive.box<Note>(noteBoxName);
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        body: Center(
      child: Container(
        width: width * 0.6,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: _titleContorller,
              decoration: const InputDecoration(hintText: 'Title'),
            ),
            TextField(
              controller: _descContorller,
              decoration: const InputDecoration(hintText: 'Desc'),
            ),
            TextField(
              controller: _tagContorller,
              decoration: const InputDecoration(hintText: 'Tags'),
            ),
            MyBottom(
                function: () {
                  var title = _titleContorller.text;
                  var desc = _descContorller.text;
                  List<String> tagsList = _tagContorller.text.split(',');
                  var timeDetail = TimeDetail(
                      noteItem == null
                          ? DateTime.now()
                          : noteItem!.timeDetail.createdAt,
                      DateTime.now());
                  var finalNote = Note(title, desc, tagsList, timeDetail);
                  noteItem == null
                      ? noteBox.add(finalNote)
                      : noteBox.putAt(widget.noteIndex!, finalNote);
                  Navigator.pop(context);
                },
                title: 'Done')
          ],
        ),
      ),
    ));
  }
}
